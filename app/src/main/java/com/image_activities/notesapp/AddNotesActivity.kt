package com.image_activities.notesapp

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import io.realm.Realm
import io.realm.kotlin.where

class AddNotesActivity : AppCompatActivity() {
    private lateinit var titleET: EditText
    private lateinit var descET:EditText
    private lateinit var saveBtn: Button
    private lateinit var realm: Realm

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_notes)

        realm = Realm.getDefaultInstance()

        //init views
        titleET = findViewById(R.id.title_EditText)
        descET = findViewById(R.id.description_EditText)
        saveBtn = findViewById(R.id.saveNotesBtn)

        saveBtn.setOnClickListener {
            addNotesToDB()
        }

    }

    private fun addNotesToDB() {
        try {
            realm.beginTransaction()

            val currentIdNumber:Number? = realm.where(Notes::class.java).max("id")
            val nextID:Int

            nextID = if(currentIdNumber==null){
                1
            }else{
                currentIdNumber.toInt()+1
            }

            val notes = Notes()
            notes.title = titleET.text.toString()
            notes.description = descET.text.toString()
            notes.id = nextID

            realm.copyToRealmOrUpdate(notes)
            realm.commitTransaction()

            Toast.makeText(this,"Success",Toast.LENGTH_SHORT).show()

            startActivity(Intent(this,MainActivity::class.java))
            finish()

        } catch (e:Exception){

            Toast.makeText(this,"Failure",Toast.LENGTH_SHORT).show()
        }


    }
}