package com.image_activities.notesapp

import android.annotation.SuppressLint
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.StaggeredGridLayoutManager
import com.google.android.material.floatingactionbutton.FloatingActionButton
import io.realm.Realm
import io.realm.RealmResults
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private lateinit var realm: Realm
    private  var notesList = ArrayList<Notes>()
    private lateinit var addNotes: FloatingActionButton
    private lateinit var notesRV : RecyclerView

    @SuppressLint("WrongConstant")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        //init realm
        realm = Realm.getDefaultInstance()
        //init views
        notesRV = findViewById<RecyclerView>(R.id.notesRV)
        addNotes = findViewById(R.id.addNotes)

//        // layout manager
//        todoRV.layoutManager = LinearLayoutManager(
//            this,
//            LinearLayout.VERTICAL, false
//        )
        notesRV.layoutManager = StaggeredGridLayoutManager(2, LinearLayoutManager.VERTICAL)
        addNotes.setOnClickListener {

            startActivity(Intent(this,AddNotesActivity::class.java))
            finish()
        }

        getAllNotes()

        addNotes.setOnLongClickListener {
            realm.beginTransaction()
            realm.deleteAll()
            realm.commitTransaction()

            getAllNotes()

            return@setOnLongClickListener true
        }

    }

    private fun getAllNotes() {
        notesList = ArrayList()

        val results: RealmResults<Notes> = realm.where<Notes>(
            Notes::class.java
        ).findAll()


        notesRV.adapter = NotesAdapter(this, results)
        notesRV.adapter!!.notifyDataSetChanged()

    }
}